const mongoose = require('mongoose')
const dev = require('./dev.config.json')

exports.connectMyDb = () => {
  mongoose.connect(
    dev.DB_CONNECT,
      { useUnifiedTopology: true, useNewUrlParser: true },
      () => console.log('connected to Database')
  )
}