const {objectId} = require("mongodb");
const User = require('../models/user')
const pass = require("../middleware/common")
const authToken = require("../middleware/auth")
const Global = require("../message")
const idExist = require("../dataproviders/customerprovider");
// const sendMail = require("../helper/emali")


//register User
exports.register = async (req, res)=> {
  const { fullname, email, password, role_id, phone_no, address } = req.body

  try{
    if(!fullname)
    throw Global.message.FULL_NAME_REQ  

    const findCustomer = await User.findOne({email : email})
    if(findCustomer && findCustomer != null){
      res.status(200).json({
        sucesss: true,
        message: Global.message.ALREAY_CREATE
      })
    }
    else{
      let mypass = pass.encryptPass(password)
      let registerCustomer = new User({
        fullname: fullname ? fullname :"", 
        email :email,
        password : mypass,
        role_id : role_id ? role_id : "user",
        phone_no : phone_no ? phone_no : {},
        address : address ? address : {}
      })
      let saveCustomer = await registerCustomer.save()
      res.status(201).json({
        success:true,
        data : saveCustomer
      })
    }
  }catch(error){
    res.status(400).json({
      success: false,
      err:error
    })
  }
}


//USER LOGIN
exports.login = async(req, res)=>{
  const { email , password } = req.body
  try{
    const findCustomer = await User.findOne({email : email})
    if(findCustomer && findCustomer != null){
      const checkPass = pass.decryptPass(findCustomer.password)
      if(password == checkPass){
        let token = authToken.createToken(findCustomer.email, findCustomer._id)
        res.status(200).json({
          success: true,
          data : findCustomer,
          token: token
        })
      }
      else  
        throw Global.message.PASSWORD_NOT_MATCH
    }
    else
      throw Global.message.NO_ACCOUNT_FOUND
  }
  catch(error){
    res.status(400).json({
      success:false,
      err:error
    })
  }
}


//UPDATE PROFILE
exports.updateProfile = async(req, res)=>{
  try{
    const {fullname, phone_no, address} = req.body
    const Id = req.params.id
    const customerId = await idExist.checkId(User, Id)

    if(customerId._id == undefined){
      throw Global.message.NO_Data_
    }
    else{
      const customerData = {
        fullname:fullname,
        phone_no: phone_no,
        address: address
      };
      await User.updateOne({id:_id}, customerData);
      res.status(200).json({
        success:true,
        message:Global.message.USER_UPDATED_SUCC
      })
    }
  }
  catch(error){
    res.status(400).json({
      success:false,
      err:error
    })
  }  
}


//UPDATE cUSTOMER BY TOKEN
exports.updateProfileByToken = async(req, res) =>{
  try{
  
    const {fullname, role_id, phone_no, address } = req.body
    const customerData = {
      fullname:fullname,
      role_id: role_id,
      phone_no:phone_no,
      address:address
    }
    await User.findByIdAndUpdate({ id: req.decoded.id}, customerData)
    res.status(200).json({
      success:true,
      message:Global.message.USER_UPDATED_SUCC
    })
  }catch(error){
    res.status(400).json({
      success:false,
      err:errror
    })
  }
}

