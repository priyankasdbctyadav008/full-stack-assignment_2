const { objectId } = require("mongodb");
const Product = require('../models/product')
const Sale = require('../models/sales')
const Global = require("../message")
const idExist = require("../dataproviders/customerprovider");


//ADD Product
exports.addProduct = async (req, res) => {
  const { name, description, quantity, price, category, brand, stock, imageUrl } = req.body

  try {
    if (!name)
      throw Global.message.FULL_NAME_REQ

    if (!req.file)
      throw Global.message.IMAGE_REQUIRED;

    const imagePath = req.file.path;
    const findProduct = await Product.findOne({ name: name })
    if (findProduct && findProduct != null) {
      res.status(200).json({
        sucesss: true,
        message: Global.message.ALREAY_CREATE
      })
    }
    else {
      let addProduct = new Product({
        name: name ? name : "",
        description: description ? description : "",
        price: price ? price : "",
        category: category ? category : "",
        brand: brand ? brand : "",
        stock: stock ? true : false,
        quantity: quantity ? quantity : "",
        imageUrl : imagePath ? imagePath: ""
      })

      let savedProduct = await addProduct.save()


      //Add Sales
      let newSale = new Sale({
        invoiceNumber: Math.random(),
        employeeId: 'emp133',
        dateOfSale: new Date(),
        discount: 0,
        vat: 0,
        invoiceTotal: price || 0,
        products: [{ id: savedProduct._id, price: price || 0 }],
      });

      let savedSale = await newSale.save();


      const sales = [];
      sales.push(savedSale._id);
      const updatedProduct = await Product.updateOne({ _id: savedProduct._id }, { sales: sales });

      res.status(201).json({
        success: true,
        data: savedProduct,
        sales: updatedProduct
      })
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      err: error
    })
  }
}

//Get Product List
exports.productList = async (req, res) => {
  try {
    const products = await Product.find().populate('sales');
    res.status(200).json({
      success: true,
      data: products
    });
  } catch (error) {
    console.error('Error fetching products:', error);
    res.status(500).json({
      success: false,
      error: 'Internal Server Error'
    });
  }
};

//Get Product by ID
exports.getProductByID = async (req, res) => {
  const productId = req.params.id;
  try {
    const product = await Product.findOne({ _id: productId }).populate('sales');
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }
    res.status(200).json(product);
  } catch (error) {
    console.error('Error fetching product by ID:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


//Get Product by Category
exports.getProductByCategory = async (req, res) => {
  const category =  req.params.category;
  console.log('category: ', category);
  try {
    const product = await Product.find({ category: category }).populate('sales');
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }
    res.status(200).json(product);
  } catch (error) {
    console.error('Error fetching product by ID:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


//UPDATE PRODUCT
exports.updateProduct = async (req, res) => {
  try {
    const { name, description, price, category, brand, stock, imageUrl } = req.body
    const Id = req.params.id


    if (Id == undefined) {
      throw Global.message.NO_Data_
    }
    else {
      const productData = {
        name: name,
        description: description,
        price: price,
        category: category,
        brand: brand,
        stock: stock,
      };
      const updatedProduct = await Product.updateOne({ _id: Id }, productData);
      res.status(200).json({
        success: true,
        message: Global.message.USER_UPDATED_SUCC,
        data: updatedProduct
      })
    }
  }
  catch (error) {
    res.status(400).json({
      success: false,
      err: error
    })
  }
}


//DELETE PRODUCT BY TOKEN
exports.deleteProduct = async (req, res) => {
  const productId = req.params.id;
  try {
    const product = await Product.findOneAndRemove({ _id: productId });
    console.log('product------------------------', product)

    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    res.status(200).json({ message: 'Product deleted successfully' });
  } catch (error) {
    console.error('Error deleting product by ID:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


