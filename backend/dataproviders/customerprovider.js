const Global = require('../message')
const { ObjectId} = require('mongodb')
const Customer = require('../models/user')
const Product = require('../models/product')



exports.checkId = async(table, id)=>{
  try{
    if(id){
      const findData = await table.findById(id)
      if(findData && findData != null){
        return findData
      }
      else
        throw Global.message.NO_Data_
    }
    else
      throw Global.message.Id_REQ
  }
  catch(error){
    return error
  }
}


