const express = require("express");
const router = express.Router()
const UserController = require('../controllers/userController');
const validate = require('../middleware/validate')
const auth = require('../middleware/auth')

router.post('/register',validate.validateEmailAndPassword, UserController.register)
router.post('/login', validate.validateEmailAndPassword, UserController.login)
router.put('/updateProfile/:id', UserController.updateProfile)
router.put('/profileUpdate',  auth.tokenValidation, UserController.updateProfileByToken)

module.exports = router;