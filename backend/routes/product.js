const express = require("express");
const router = express.Router()
const ProductController = require('../controllers/productController');
const validate = require('../middleware/validate')
const auth = require('../middleware/auth')
const { upload } = require('../middleware/upload');

router.post('/addProduct', upload.single('imageUrl'), ProductController.addProduct)
router.get('/productList',ProductController.productList)
router.get('/getProduct/:id', ProductController.getProductByID)
router.delete('/deleteProduct/:id', ProductController.deleteProduct)
router.put('/updateProduct/:id',ProductController.updateProduct)
router.get('/getProductByCategory/:category',ProductController.getProductByCategory)

module.exports = router;