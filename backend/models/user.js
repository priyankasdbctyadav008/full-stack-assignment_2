const moongose = require('mongoose');
const Schema = moongose.Schema

const userSchema = new Schema({

  fullname:{  
    type:String,
  },

  email:{
    type: String,
    unique :true,
    required :true
  },

  password:{
    type:String,
  },

  phone_no:{
    type: String,
  },

  address:{
    type: Object,
  },

  status:{
    type:Boolean,
  },

  image:{
    type:String
  },

  role_id: {
    type: String,
    enum: ['admin', 'manager'], 
    default: 'manager'
  },
},
    {timestamps:true}
)

let User = moongose.model('user', userSchema);

module.exports =  User