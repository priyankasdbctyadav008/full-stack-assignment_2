const mongoose = require('mongoose');

const saleSchema = new mongoose.Schema({
  invoiceNumber: {
    type: String,
    required: true,
    unique: true,
  },
  employeeId: {
    type: String,
    required: true,
  },
  dateOfSale: {
    type: Date,
    required: true,
  },
  discount: {
    type: Number,
    default: 0,
    min: 0,
  },
  vat: {
    type: Number,
    default: 0,
    min: 0,
  },
  invoiceTotal: {
    type: Number,
    required: true,
    min: 0,
  },
});



module.exports = mongoose.model('Sales', saleSchema);
