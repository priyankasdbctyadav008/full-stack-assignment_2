const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
    min: 0,
  },
  price: {
    type: Number,
    required: true,
    min: 0,
  },
  category: {
    type: String,
    required: true,
    trim: true,
  },
  brand: {
    type: String,
    trim: true,
  },
  stock: {
    type: Boolean,
    default: true,
  },
  imageUrl: {
    type: String,
    // validate: {
    //   validator: (value) => {
    //     const urlRegex = /^(https?:\/\/)?([a-zA-Z0-9-_.]+)([.][a-zA-Z0-9]{2,})+([a-zA-Z0-9-/_]*)*$/;
    //     return urlRegex.test(value);
    //   },
    //   message: 'Invalid image URL',
    // },
  },
  sales: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Sales',
    },
  ],
});

module.exports = mongoose.model('Product', productSchema);
