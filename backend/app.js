require('dotenv').config();
const express = require("express")
const bodyParser = require('body-parser')
const cors = require('cors');
const morgan = require('morgan')
const path = require('path')
const app = express();

const dbConfig = require('./config/db.config')
dbConfig.connectMyDb()

app.use(cors())
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api/uploads', express.static('uploads'));

app.set('view engine', 'ejs');
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
const port = 8000


app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Content-Length, Authorization, Accept, X-Requested-With");
  res.header('Access-Control-Expose-Headers', 'Content-Disposition');
  next();
})

app.use('/api', require('./routes/index'))

app.get("/", (req, res)=>{
  res.json({ message: "Welcome ........." });
})


app.listen(port, ()=>{
  console.log(`Server running on  ${port}`)
})