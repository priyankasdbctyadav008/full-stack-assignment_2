const Cryptojs = require('crypto-js')
const dev = require("../config/dev.config.json")


exports.decryptPass = (dPassword) => {
  let bytes = Cryptojs.AES.decrypt(dPassword, dev.PASSWORD_SECRET_KEY);
  let password = bytes.toString(Cryptojs.enc.Utf8);
  return password
}

exports.encryptPass = (ePassword) => {
  let password = Cryptojs.AES.encrypt(ePassword, dev.PASSWORD_SECRET_KEY).toString();
  return password
}