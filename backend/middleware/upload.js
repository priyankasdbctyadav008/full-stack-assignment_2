
const multer = require('multer');
const path = require('path');

//Store file in local device
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/products'); 
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const extension = path.extname(file.originalname);
    cb(null, uniqueSuffix + extension);
  },
});

exports.upload = multer({ storage: storage });