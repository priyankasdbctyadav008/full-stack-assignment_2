const jwt = require('jsonwebtoken')
const dev = require('../config/dev.config.json')

let jwtSecretKey = dev.JWT_SECRET_KEY

exports.createToken = ( email, id) =>{
  let data = {
    email : email,
    id:id 
  }
 const token = jwt.sign(data, jwtSecretKey, {expiresIn: '8h'})
 return token 
}

exports.tokenValidation = (req, res, next) => {
  try {
    let token = req.body.token || req.query.token || req.headers["x-access-token"] || req.headers["authorization"];
    if(!token){
      return res.status(403).send("A token is required for authentication");
    }
    token = token.replace("Bearer", "")
    const temp = jwt.verify(token, jwtSecretKey)
    req.decoded = temp
    next()

  }catch(err){
    console.log('Error:', err)
  }
}