Creating a complete Point of Sale (POS) system with all the described features would be a significant project, and providing a full implementation in this text format is not feasible. However, I can provide you with an outline of the steps and components you would need to build such a system. You can use this outline as a starting point and implement each feature incrementally.

Here's a high-level outline of the steps and components for building a POS system:

1. **User Roles:**
   - Define two user roles: Admin and Manager.

2. **Authentication:**
   - Implement user authentication to distinguish between Admin and Manager users.

3. **Admin Functionality:**
   - Admin can add products with details like Name, Category, Unit Price, Quantity, and Image.

4. **Manager Functionality:**
   - Manager can log in and view the product list added by the Admin.
   - The product list should be categorized by their category.

5. **Product Listing:**
   - Display the product list with details.
   - Enable hover-over tooltips to show price and description.

6. **Shopping Cart:**
   - Implement a shopping cart on the left panel.
   - Allow Manager to drag and drop products from the product list to the cart.
   - Show a message when the cart is empty.

7. **Cart Operations:**
   - Update the cart's total cost as products are added.
   - Allow quantity adjustments and remove items from the cart.
   - Automatically update subtotal and total when cart contents change.

8. **Discounts:**
   - Admin can apply discounts in percentage to products.
   - Calculate the discount on the subtotal.

9. **VAT Calculation:**
   - Calculate VAT (Value Added Tax) on the subtotal.
   - Ensure that discounts do not impact VAT calculation.

10. **Total Calculation:**
    - Calculate the total as Subtotal + VAT - Discount.

11. **Sale Processing:**
    - Allow the user to process a sale.
    - Generate a receipt with:
      - Invoice number
      - Employee ID
      - Date of sale
      - List of products sold (with prices)
      - Discount amount
      - VAT amount
      - Invoice total

12. **Database Integration:**
    - Save sales data (invoices) to a database with the mentioned details.

13. **Inventory Management:**
    - Update the available quantity of products after a sale is made.
    - Ensure that the system handles stock availability.

14. **Cancel Sale:**
    - Allow the user to cancel a sale, which clears the cart.

15. **Reset and Cleanup:**
    - Implement functionality to reset the system and return to the initial state after a sale is completed or canceled.
    - Provide a close button for the receipt.

16. **User Interface (UI):**
    - Design an intuitive and user-friendly interface for both Admin and Manager.

17. **Front-End and Back-End Integration:**
    - Connect the front-end (UI) with the back-end (server) to handle user actions, data retrieval, and database operations.

18. **Testing:**
    - Thoroughly test the system to ensure all features work as expected.

19. **Deployment:**
    - Deploy the POS system to a web server or cloud hosting platform.

20. **Security:**
    - Implement security measures to protect user data, such as encryption and authentication.

21. **Error Handling:**
    - Handle errors gracefully and provide informative error messages to users.

This is a complex project, and you may need to break it down into smaller tasks and implement them one by one. Additionally, you may want to consider using a web development framework like Express.js for the back end and a front-end framework like React or Angular to help you build the user interface more efficiently.

Remember to plan your project, create a database schema, and thoroughly test each feature as you develop it. You can refer to relevant documentation and tutorials for specific technologies and libraries you plan to use in your project.