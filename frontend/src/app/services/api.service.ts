import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private router: Router,
    private httpService: HttpService,
    private http: HttpClient

  ) { }

  loginUser(user: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.post('user/login', user).subscribe({
          next: (res) => {
            let demo = JSON.stringify(res);
            localStorage.setItem('token', JSON.parse(demo).token);
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        
        reject(error);
      }
    });
  }

  token() {
    let data: any = localStorage.getItem('token');
    if (data) {
      return data;
    }
  }

  isLoggedIn() {
    if (this.token()) {
      return true;
    } else {
      return false;
    }
  }

  getProduct(){
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get('product/productList', '').subscribe({
          next: (res) => {
            console.log('res: ', res);
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        
        reject(error);
      }
    });
  }

  getProductByCategory(cat:any){
    return new Promise(async (resolve, reject) => {
      try {
        await this.httpService.get(`product/getProductByCategory/${cat}`, '').subscribe({
          next: (res) => {
            console.log('res: ', res);
            resolve(res);
          },
          error: (error) => {
            reject(error);
          },
        });
      } catch (error) {
        
        reject(error);
      }
    });
  }
}