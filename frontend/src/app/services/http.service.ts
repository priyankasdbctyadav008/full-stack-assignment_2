import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private api = environment.apiUrl;

  constructor(private http: HttpClient) { }

  post(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.post(fullUrl, data)
  }

  get(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.get(fullUrl, data);
  }


  delete(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.delete(fullUrl, data);
  }

  put(url: string, data: any) {
    const fullUrl = this.api + url;
    return this.http.put(fullUrl, data);
  }
}
