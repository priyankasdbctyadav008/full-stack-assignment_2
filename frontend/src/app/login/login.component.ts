import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;
  data: any;

  constructor(private fb: FormBuilder, public authService: AuthService, private router: Router) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  logIn() {
    console.log(' this.loginForm.value.email: ',  this.loginForm.value.email);
    if (this.loginForm.valid) {
      this.loginForm.value.email = this.loginForm.value.email.toLowerCase();
      try {
        this.authService.loginUser(this.loginForm.value).then(data => {
          localStorage.setItem('user', JSON.stringify(data.user));
          this.router.navigate(['product/list']);
        }, (error) => {
          console.error('Password is incorrect', 'false');
        })
      }
      catch (error: any) {
        console.error('Enter Correct Details', 'false');
      }
    }
  }
}
