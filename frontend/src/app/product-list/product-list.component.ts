import { Component, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent {
  getProductList: any[] = []
  addToCartList: any[] = [];
  totalCartPrice: any
  backendUrl = 'http://localhost:8000/api/';
  category = [
    {
      id: 1,
      name: "Computers",
      isActive: false
    },
    {
      id: 2,
      name: "Fruits",
      isActive: false
    },
    {
      id: 3,
      name: "Clothing",
      isActive: false
    },
    {
      id: 4,
      name: "Services",
      isActive: false
    }, {
      id: 5,
      name: "Burger",
      isActive: false
    },
    {
      id: 6,
      name: "Pizza",
      isActive: false
    }
  ]


  @ViewChild('confirmationModal') confirmationModal: any;
  constructor(private fb: FormBuilder, public authService: AuthService, private router: Router, private toastr: ToastrService, private modalService: NgbModal) {

  }
  ngOnInit() {
    try {
      let data: any = localStorage.getItem('addToCartList') || [];
      this.addToCartList = JSON.parse(data)
    } catch (error) {
      console.error('Error while parsing data from localStorage:', error);
      this.addToCartList = []; // Initialize with an empty array in case of an error
    }
    this.getProduct();
  }

  getProduct() {
    try {
      this.authService.getProduct().then((res: any) => {
        // let response = res.data
        this.getProductList = res?.data
        console.log('this.getProductList: ', this.getProductList);
      }, (error) => {
        console.error('data ', 'false');
      })
    }
    catch (error: any) {
      console.error('Enter Correct Details', 'false');
    }
  }

  // Function to transform imageUrl into a valid URL
  getImageUrl(product: any): string {
    const imageUrl = product.imageUrl.replace(/\\/g, '/');
    return this.backendUrl + imageUrl
  }

  toggleCategoryActive(cat: any): void {
    // Set isActive to true for the clicked category and false for all others
    this.category.forEach((value) => {
      if (value.id === cat.id) {
        console.log('cat.isActive: ', cat.isActive);
        cat.isActive = !cat.isActive;
      } else {
        value.isActive = false; // Set isActive to false for all other categories
      }
    });
    try {
      this.authService.getProductByCategory(cat.name).then((res: any) => {
        this.getProductList = res;
        console.log('this.getProductList: ', this.getProductList);
      }, (error) => {
        console.error('data ', 'false');
      })
    }
    catch (error: any) {
      console.error('Enter Correct Details', 'false');
    }
  }


  // addToCart(data: any) {
  //   this.addToCartList.map((value: any) => {
  //     if (value._id != data._id) {
  //       this.addToCartList.push(data)
  //     }
  //   })
  //   console.log('this.addToCartList: ', this.addToCartList);
  // }

  addToCart(data: any) {
    let existingItem: any
    if (this.addToCartList) {
      existingItem = this.addToCartList.find((item: any) => item._id === data._id);
    }

    if (existingItem) {
      existingItem.quantity += 1;
      existingItem.totalPrice = existingItem.price * existingItem.quantity;
    } else {
      let newItem: any = { ...data, quantity: 1, totalPrice: data.price };
      console.log('newItem: ', newItem);
      this.addToCartList.push(newItem);
    }
    this.calculateTotalCartPrice();
    localStorage.setItem('addToCartList', JSON.stringify(this.addToCartList));
    console.log('this.addToCartList: ', this.addToCartList);
  }

  calculateTotalCartPrice() {
    let total = 0;

    this.addToCartList.forEach((item: any) => {
      total += item.totalPrice;
    });

    this.totalCartPrice = total;
  }

  removeItem(itemId: number) {
    const itemIndex = this.addToCartList.findIndex((item: any) => item._id === itemId);

    if (itemIndex !== -1) {
      this.addToCartList.splice(itemIndex, 1);
    }
    localStorage.setItem('addToCartList', JSON.stringify(this.addToCartList));
  }

  cancelSale() {
    // this.toastr.error('everything is broken', 'Major Error', {
    //   timeOut: 3000,
    // });
  }

  procesSale() {
    let keyToRemove = "addToCartList";
    localStorage.removeItem(keyToRemove);
  }
}

